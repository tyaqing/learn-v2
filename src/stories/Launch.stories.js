import Launch from './Launch.vue';

export default {
    component: Launch,
    title: 'Components/Launch',
};

export const Primary = () => ({
    components: { Launch },
    template: '<Launch primary label="Launch" />',
});

Primary.storyName = '一个基本组件';
