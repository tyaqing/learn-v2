import Button from './Button.vue';

export default {
  component: Button,
  title: 'Components/Button',
};

export const Primary = () => ({
  components: { Button },
  template: '<Button primary label="Button" />',
});

Primary.storyName = '一个基本组件';
