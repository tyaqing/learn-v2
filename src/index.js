function Parent (name) {
    console.log('执行了几次')
    this.name = name;
    this.colors = ['red', 'blue', 'green'];
}
Parent.prototype.sayHello = function (){
    console.log('hello')
}


function Child (name, age) {
    Parent.call(this, name);
    this.age = age;
}

function createObject(o){
    function F(){}
    F.prototype = o
    return new F()
}

function extend(A,B){
    A.prototype = createObject(B.prototype)
    A.prototype.constructor = A
}

extend(Child,Parent)
const c = new Child('bob',26)
console.log(c)
