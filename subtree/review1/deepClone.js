function shallowCopy(obj) {
    if (typeof obj !== 'object') return null
    let r = {}
    for (let [k, v] of Object.entries(obj)) {
        r[k] = v
    }
    return r
}

function deepCopy(obj, hash = new WeakMap()) {
    if (typeof obj !== 'object') return null
    if(hash.has(obj)) return hash.get(obj)
    
    let r = obj instanceof Array ? [] : {}
    hash.set(obj, r);

    for (let [k, v] of Object.entries(obj)) {
        r[k] = typeof v === 'object' ? deepCopy(v, hash) : v
    }
    return r
}

const o = {
    a: 1,
    b: [1, 2, {a: 1}],
}
o.cir = o

let c = deepCopy(o)


console.log(c)
