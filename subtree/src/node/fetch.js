const request = require('request')

function fetch(url) {
  return new Promise((resolve) => {
    request(
      url,
      {
        headers: {
          'User-Agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
        }
      },
      (error, response, body) => {
        resolve(body)
      }
    )
  })
}

export default fetch
