const EventEmitter = require('events')

class MyEventEmitter extends EventEmitter {}
const MyEmitter = new MyEventEmitter()
MyEmitter.on('ee', () => {
  console.log('触发')
})

MyEmitter.emit('ee')
