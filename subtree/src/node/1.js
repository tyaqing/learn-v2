const obj = {
  cname: 'window'
}

Function.prototype.call2 = function (context, ...arg) {
  context.fn = this
  const res = context.fn(...arg)
  delete context.fn
  return res
}

Function.prototype.bind2 = function (context, ...arg) {
  const self = this
  const E = function () {}
  const F = function (...arg2) {
    self.call2(this instanceof F ? this : context, ...arg.concat(arg2))
  }
  E.prototype = this.prototype
  F.prototype = new E()
  return F
}

function Instance(F, ...arg) {
  const emptyObj = {}
  obj.__proto__ = F.prototype
  const res = F.call(emptyObj, ...arg)
  return typeof res === 'object' ? res : obj
}

function print(age, sex) {
  console.log(age, sex)
  console.log(this.cname)
  this.box = 'nnnn'
  this.version = 'nnnn'
  return 'bobo'
}
print.prototype.pp = 'print'

// console.log(print.call2(obj, 12, 'boy'))

const NewPrint = print.bind2(obj, 22)
NewPrint('lady')

NewPrint.prototype.pp = 'NewPrint'
// const p = new NewPrint(90, 'old')
// console.log(p)
console.log('-----------------')
const p2 = Instance(NewPrint, 34, 'func')
console.log(p2)
