var fetch = require('node-fetch');

function* gen() {
    var r1 = yield fetch('https://api.github.com/users/github');
    var r2 = yield fetch('https://api.github.com/users/github/followers');
    var r3 = yield fetch('https://api.github.com/users/github/repos');
}

// const i = gen()
//
// i.next().value.then(() => {
//     return i.next().value
// }).then(() => {
//     i.next()
// })

function run(gen) {
    const i = gen()
    function next(data) {
        const result = i.next(data)
        if(result.done) return
        result.value.then((r) => {
            next(r)
        })
    }
    next()
}
run(gen)

// function run(gen) {
//     var g = gen();
//     function next(data) {
//         var result = g.next(data);
//         console.log(result,'result')
//         if (result.done) return;
//         result.value.then(function(data) {
//             next(data);
//         });
//     }
//     next();
// }
//
// run(gen);