// import './apply'
// import './bind'
// import './new'
const obj = {
  xname: 'windowObject'
}

Function.prototype.call2 = function (context, ...arg) {
  context.fn = this
  const res = context.fn(...arg)
  delete context.fn
  return res
}
Function.prototype.bind2 = function (context, ...arg) {
  const self = this
  const E = function () {}
  const F = function (...arg2) {
    return self.call(this instanceof F ? this : context, ...arg.concat(arg2))
  }
  E.prototype = this.prototype
  F.prototype = new E()
  return F
}
function print(age, sex) {
  console.log(this.xname, age, sex)
  this.age = age
  this.sex = sex
  return this.name + 1
}
print.prototype.cname = 'printfun'

function nNew(func, ...arg) {
  const newObj = {}
  newObj.__proto__ = func.prototype
  const res = func.call2(newObj, ...arg)
  return typeof res === 'object' ? res : newObj
}

const NewPrint = print.bind2(obj)
NewPrint.prototype.cc = 'ccname'
// NewPrint('lady')

const p = nNew(NewPrint, 12, 'sss')
console.log(p)

// const p = nNew(print, 22, 'boy')

// console.log(p)
