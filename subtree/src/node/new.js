function news(...rest) {
  const obj = {}
  const Construct = [].shift.call(rest)
  // eslint-disable-next-line no-proto
  obj.__proto__ = Construct.prototype
  const res = Construct.apply(obj, rest)

  return typeof res === 'object' ? res : obj
}

function Person(name) {
  this.name = name
}
Person.prototype.age = 123

const p = news(Person, 'bob')

console.log(p)
