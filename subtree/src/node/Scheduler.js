class Scheduler {
  constructor() {
    this.taskList = []
    this.count = 0
    this.maxNum = 2
  }

  async add(promiseCreator) {
    // 如果正在执行的超过最大，就将任务推进taskList
    if (this.count >= this.maxNum) {
      await new Promise((resolve) => {
        this.taskList.push(resolve)
      })
    }
    this.count += 1
    const result = await promiseCreator()
    this.count -= 1
    if (this.taskList.length > 0) {
      // 当任务已经完成并且还有任务，把taskList弹出来继续执行，这样代码会重新执行一次
      const task = this.taskList.shift()
      task()
    }
    return result
  }
}

const timeout = (time) =>
  new Promise((resolve) => {
    setTimeout(resolve, time)
  })

const scheduler = new Scheduler()
const addTask = (time, order) => {
  scheduler.add(() => timeout(time)).then(() => console.log(order))
}

addTask(1000, '1')
addTask(500, '2')
addTask(300, '3')
addTask(400, '4')
