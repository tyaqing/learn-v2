function watch(target, func) {
  return new Proxy(target, {
    get(obj, props) {
      return obj[props]
    },
    set(obj, props, newVal) {
      obj[props] = newVal
      func(props, newVal)
    }
  })
}

const data = watch({ age: 12 }, (_, val) => {
  document.getElementById('span').innerHTML = val
})

document.getElementById('button').addEventListener('click', () => {
  data.age += 1
})
document.getElementById('set').addEventListener('click', () => {
  data.age -= 1
})
