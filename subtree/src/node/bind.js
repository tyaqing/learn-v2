const foo = { name: 'kevin' }

function print(name, age) {
  console.log(name, age)
  console.log(this, 'this!!!')
  console.log(this.name)
  return name + age
}
print.prototype.app = '1123'

// const newPrint = print.bind(foo)

// newPrint()

// eslint-disable-next-line no-extend-native
Function.prototype.bind2 = function bind2(context, ...arg) {
  // 上下文存入当前函数
  const self = this
  // 返回函数变量
  const F = function f(...arg2) {
    // 执行当前函数 返回结果    如果是构造函数,则让this指向当前函数的上下文
    return self.call(this instanceof F ? this : context, ...arg.concat(arg2))
  }
  // 新建一个函数对象,避免影响F本身的prototype
  const Empty = function () {}
  // 如果返回构造函数,需要将prototype继承给新的实例
  Empty.prototype = this.prototype
  F.prototype = new Empty()
  return F
}

const fooPrint = print.bind2(foo, 'good')

fooPrint.prototype.app = '2222'
// console.log(fooPrint('hah'))
console.log('------')
// eslint-disable-next-line new-cap
const p = new fooPrint('bob', 22)
console.log(p)
