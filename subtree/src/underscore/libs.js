export default ''
/**
 * 防抖函数
 * @param {Function} func 需要防抖的函数
 * @param {Number} delay 延迟
 * @param {Boolean} immediate 是否立即执行
 * @returns
 */
export const debounce = function (func, delay, immediate) {
  let timmer
  let result
  const deb = function (...arg) {
    if (timmer) clearTimeout(timmer)
    if (immediate) {
      const callNow = !timmer
      timmer = setTimeout(() => {
        timmer = null
      }, delay)
      if (callNow) result = func.call(this, ...arg)
    } else {
      timmer = setTimeout(() => {
        func.call(this, ...arg)
      }, delay)
    }
    return result
  }
  deb.cancel = function () {
    clearTimeout(timmer)
    timmer = null
  }
  return deb
}
/**
 * 节流
 * @param {*} func 函数
 * @param {*} wait 等待时间
 * @param {*} config 头尾设置
 * @returns
 */

export const throttle = function (func, wait, config) {
  // 做配置判断
  const defaultConifg = {
    trailing: true,
    leading: true
  }
  const { trailing, leading } = { ...defaultConifg, ...config }
  if (!trailing && !leading) throw new Error('trailing,length不能同时设为false')
  let timeout
  let previous = 0
  return function (...arg) {
    const now = +new Date()
    if (!leading) previous = now
    const remaining = wait - (now - previous)
    console.log(remaining)
    // 如果没有剩余时间则立即执行函数
    if (remaining <= 0) {
      if (timeout) {
        clearTimeout(timeout)
        timeout = null
      }
      func.call(this, ...arg)
      previous = +new Date()
    } else if (!timeout && trailing) {
      timeout = setTimeout(() => {
        func.call(this, ...arg)
        previous = +new Date()
        timeout = null
      }, remaining)
    }
  }
}
/**
 * 深拷贝
 * @param {*} obj
 * @returns
 */
export const deepCopy = (obj) => {
  if (typeof obj !== 'object') return null
  const newObj = obj instanceof Array ? [] : {}
  Object.keys(obj).forEach((key) => {
    newObj[key] = typeof obj[key] === 'object' ? deepCopy(obj[key]) : obj[key]
  })
  return newObj
}
/**
 * 继承
 * @param {*} isDeepCopy
 * @param  {...any} args
 * @returns
 */
export const extend = (isDeepCopy = false, ...args) => {
  let obj
  if (isDeepCopy === true) {
    ;[obj] = args
    args.shift()
  } else {
    obj = isDeepCopy
  }
  args.forEach((objItem) => {
    Object.keys(objItem).forEach((key) => {
      // 避免循环引用
      if (objItem[key] === obj) return
      if (objItem[key] !== undefined) {
        obj[key] = isDeepCopy === true ? deepCopy(objItem[key]) : objItem[key]
      }
    })
  })
  return obj
}
/**
 * 扁平化数组
 * @param {*} array
 * @returns
 */
export const flatten = (array) =>
  array.reduce(
    (pre, cur) => pre.concat(Array.isArray(cur) ? flatten(cur) : cur),
    []
  )
/**
 * 柯里化
 * @param {*} fn
 * @param  {...any} args
 * @returns
 */
export const curry = function (fn, ...args) {
  const { length } = fn
  return function (...innerArgs) {
    const allArgs = args.concat(innerArgs)
    if (allArgs.length < length) {
      return curry.call(this, fn, ...allArgs)
    }
    return fn.call(this, ...allArgs)
  }
}
/**
 * 局部函数
 * @param {*} fn
 * @param  {...any} args
 * @returns
 */
export const partial = (fn, ...args) =>
  function (innerArgs) {
    return fn.call(this, ...args.concat(innerArgs))
  }

/**
 *  随机数组
 * @param {*} a
 * @returns
 */
export const shuffle = (a) => {
  for (let i = a.length; i >= 0; i -= 1) {
    const j = Math.floor(Math.random() * i)
    ;[a[i - 1], a[j]] = [a[j], a[i - 1]]
  }
  return a
}
