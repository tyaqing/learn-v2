// 尾递归实践
function factorial(n, res = 1) {
  if (n === 1) return res
  return factorial(n - 1, res * n)
}

console.log(factorial(5))

function fibonacci(n, sum1 = 1, sum2 = 1) {
  if (n === 1) return 0
  return n <= 2 ? sum2 : fibonacci(n - 1, sum2, sum1 + sum2)
}
console.log(fibonacci(12))

console.log('love'[2])
