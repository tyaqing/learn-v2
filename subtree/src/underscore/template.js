const obj = {
  name: 'bob',
  age: 12,
  fav: ['football', 'table']
}
//
// function handle(literals, ...values) {
//   return literals.reduce((prev, next, index) => {
//     console.log(index)
//     const value = values[index - 1]
//     return prev + value.toString().toLocaleUpperCase() + next
//   })
// }

// console.log(
//   handle`I'm ${obj.name},${obj.age} years old. My favors is ${obj.fav
//     .map((item) => item)
//     .join(',')}.`
// )

function oneLine(template, ...expressions) {
  let result = template.reduce((prev, next, i) => {
    const expression = expressions[i - 1]
    return prev + expression + next
  })

  result = result.replace(/(\n\s*)/g, ' ')
  result = result.trim()

  return result
}
console.log(oneLine`    
    Hi,
    Daisy!
    I am
    Kevin.`)
