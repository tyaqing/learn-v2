function _(obj) {
  // eslint-disable-next-line new-cap
  if (!(this instanceof _)) return new _(obj)
  this._wraps = obj
}
// 遍历所有的函数 返回函数名称数组
_.functions = function (obj) {
  const funcs = []
  for (const key in obj) {
    if (typeof obj[key] === 'function') funcs.push(key)
  }
  return funcs
}

_.chain = function (obj) {
  const instance = _(obj)
  instance._chain = true
  return instance
}
_.prototype.push = function (num) {
  this._wraps.push(num)
  return this
}
_.prototype.shift = function () {
  this._wraps.shift()
  return this
}
_.prototype.shift = function () {
  this._wraps.shift()
  return this
}
_.prototype.value = function () {
  return this._wraps
}
// 将underscore的函数放入到新的实例中去
_.mixin = function (obj) {
  _.functions(obj).forEach((name) => {
    const func = (_[name] = obj[name])
    _.prototype[name] = function (arg) {
      // 这里 context是underscore 是因为this需要访问underscore的成员变量和函数
      // 如果是this的话， allarg 的参数会有问题
      const allArg = [this._wraps].concat([arg])
      return func.apply(_, allArg)
    }
  })
  return _
}
_.mixin(_)

// underscore.version('yakir')

// underscore('laige').version('add').version()
const res = _.chain([1, 2, 3]).push(4).shift().push(9).value()
