// const isFunction = (f) => typeof f === 'function'
// // Promise 3种状态
// const PENDING = 'PENDING'
// const FULFILLED = 'FULFILLED'
// const REJECTED = 'REJECTED'
//
// class MyPromise {
//   _status = PENDING
//   _value = undefined
//   _fulfilledQueue = []
//   _rejectedQueue = []
//
//   constructor(executor) {
//     if (!isFunction(executor))
//       throw new Error('MyPromise must accept a function as a parameter')
//     try {
//       // 同步执行Promise包裹的代码
//       executor(this._resolve.bind(this), this._reject.bind(this))
//     } catch (e) {
//       console.error(e)
//       this._reject(e)
//     }
//   }
//   // 添加resolve时执行的函数
//   _resolve(val) {
//     const run = () => {
//       if (this._status !== PENDING) return
//       // 依次执行成功队列中的函数，并清空队列
//       const runFulfilled = (value) => {
//         while (this._fulfilledQueue.length) {
//           // 每个p1 实例都要去告知他当前的状态
//           this._fulfilledQueue.shift()(value)
//         }
//       }
//       // 依次执行失败队列中的函数，并清空队列
//       const runRejected = (error) => {
//         while (this._rejectedQueue.length) {
//           this._rejectedQueue.shift()(error)
//         }
//       }
//       /* 如果resolve的参数为Promise对象，则必须等待该Promise对象状态改变后,
//         当前Promise的状态才会改变，且状态取决于参数Promise对象的状态
//       */
//       if (val instanceof MyPromise) {
//         // 手动去then执行
//         val.then(
//           (value) => {
//             this._value = value
//             this._status = FULFILLED
//             runFulfilled(value)
//           },
//           (err) => {
//             this._value = err
//             this._status = REJECTED
//             runRejected(err)
//           }
//         )
//       } else {
//         this._value = val
//         this._status = FULFILLED
//         runFulfilled(val)
//       }
//     }
//     // 为了支持同步的Promise，这里采用异步调用
//     setTimeout(run, 0)
//   }
//
//   // 添加reject时执行的函数
//   _reject(err) {
//     if (this._status !== PENDING) return
//     // 依次执行失败队列中的函数，并清空队列
//     const run = () => {
//       this._status = REJECTED
//       this._value = err
//       while (this._rejectedQueue.length) {
//         this._rejectedQueue.shift()(err)
//       }
//     }
//     // 为了支持同步的Promise，这里采用异步调用
//     setTimeout(run)
//   }
//
//   then(onFulfilled, onRejected) {
//     const { _value, _status } = this
//     // 返回一个新的Promise对象
//     return new MyPromise((resolve, reject) => {
//       // 封装一个成功时执行的函数
//       const fulfilled = (value) => {
//         try {
//           if (!isFunction(onFulfilled)) {
//             // 当参数没有提供函数,直接将值传递到下一个地方
//             resolve(value)
//           } else {
//             const res = onFulfilled(value)
//             if (res instanceof MyPromise) {
//               // 如果当前回调函数返回MyPromise对象，必须等待其状态改变后在执行下一个回调
//               res.then(resolve, reject)
//             } else {
//               // 否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
//               resolve(res)
//             }
//           }
//         } catch (err) {
//           // 如果函数执行出错，新的Promise对象的状态为失败
//           reject(err)
//         }
//       }
//       // 封装一个失败时执行的函数
//       const rejected = (error) => {
//         try {
//           if (!isFunction(onRejected)) {
//             reject(error)
//           } else {
//             const res = onRejected(error)
//             // 因为reject只是换了个函数执行,如果reject返回值,同样会进入下一个then的resolve
//             if (res instanceof MyPromise) {
//               // 如果当前回调函数返回MyPromise对象，必须等待其状态改变后在执行下一个回调
//               res.then(resolve, reject)
//             } else {
//               // 否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
//               resolve(res)
//             }
//           }
//         } catch (err) {
//           // 如果函数执行出错，新的Promise对象的状态为失败
//           reject(err)
//         }
//       }
//       switch (_status) {
//         // 当状态为pending时，将then方法回调函数加入执行队列等待执行
//         case PENDING:
//           this._fulfilledQueue.push(fulfilled)
//           this._rejectedQueue.push(rejected)
//           break
//         // 当状态已经改变时，立即执行对应的回调函数
//         case FULFILLED:
//           fulfilled(_value)
//           break
//         case REJECTED:
//           rejected(_value)
//           break
//         default:
//       }
//     })
//   }
//   // catch也可以在下次进入resolve
//   catch(onRejected) {
//     return this.then(undefined, onRejected)
//   }
//   static resolve(value) {
//     // 如果是promise 直接返回, 否则返回新的promise对象
//     if (value instanceof MyPromise) return value
//     return new MyPromise((resolve) => resolve(value))
//   }
//   static reject(value) {
//     return new MyPromise((resolve, reject) => reject(value))
//   }
//   static all(list) {
//     return new MyPromise((resolve, reject) => {
//       /**
//        * 返回值的集合
//        */
//       const values = []
//       let count = 0
//       // eslint-disable-next-line no-restricted-syntax
//       for (const [i, p] of list.entries()) {
//         // 数组参数如果不是MyPromise实例，先调用MyPromise.resolve
//         this.resolve(p).then(
//           // eslint-disable-next-line no-loop-func
//           (res) => {
//             values[i] = res
//             count += 1
//             // 所有状态都变成fulfilled时返回的MyPromise状态就变成fulfilled
//             if (count === list.length) resolve(values)
//           },
//           (err) => {
//             // 有一个被rejected时返回的MyPromise状态就变成rejected
//             reject(err)
//           }
//         )
//       }
//     })
//   }
//   static race(list) {
//     return new MyPromise((resolve, reject) => {
//       // eslint-disable-next-line no-restricted-syntax
//       for (const p of list) {
//         // 只要有一个实例率先改变状态，新的MyPromise的状态就跟着改变
//         this.resolve(p).then(
//           (res) => {
//             resolve(res)
//           },
//           (err) => {
//             reject(err)
//           }
//         )
//       }
//     })
//   }
//   finally(cb) {
//     return this.then(
//       (value) => MyPromise.resolve(cb()).then(() => value),
//       (reason) =>
//         MyPromise.resolve(cb()).then(() => {
//           throw reason
//         })
//     )
//   }
// }
// module.exports = MyPromise
