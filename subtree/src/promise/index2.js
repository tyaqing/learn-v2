// const PENDING = 'pending'
// const FULFILLED = 'fulfilled'
// const REJECTED = 'rejected'
//
// const isFunction = (f) => typeof f === 'function'
//
// class myPromise {
//     _status = PENDING
//     _value = null
//     _fulfilledQueues = []
//     _rejectedQueues = []
//
//     constructor(executor) {
//         if (!isFunction(executor)) {
//             throw new Error('executor not is function')
//         }
//         try {
//             executor(this._resolve.bind(this), this._reject.bind(this))
//         } catch (e) {
//             this._reject(e)
//         }
//     }
//
//     _resolve(result) {
//         const run = () => {
//             // TODO 这里缺了防止二次resolve的判断
//             if (this._status !== PENDING) return
//
//             const fulfilled = (val) => {
//                 while (this._fulfilledQueues.length) {
//                     this._fulfilledQueues.shift()(val)
//                 }
//             }
//             const rejected = (err) => {
//                 while (this._rejectedQueues.length) {
//                     this._rejectedQueues.shift()(err)
//                 }
//             }
//             if (result instanceof myPromise) {
//                 result.then((val) => {
//                     this._value = val
//                     this._status = PENDING
//                     fulfilled(val)
//                 }, err => {
//                     this._value = err
//                     this._status = REJECTED
//                     rejected(err)
//                 })
//             } else {
//                 this._value = result
//                 this._status = FULFILLED
//                 fulfilled(result)
//             }
//         }
//         setTimeout(run)
//     }
//
//     _reject(err) {
//         const run = () => {
//             // TODO 这里缺了防止二次resolve的判断
//             if (this._status !== PENDING) return
//
//             while (this._rejectedQueues.length) {
//                 this._rejectedQueues.shift()(err)
//             }
//             this._status = REJECTED
//             this._value = err
//         }
//         setTimeout(run)
//     }
//
//     then(onResolve, onReject) {
//         return new myPromise((resolve, reject) => {
//             const fulfilled = (value) => {
//                 try {
//                     if (!isFunction(onResolve)) resolve(value);
//                     const res = onResolve(value);
//                     if (res instanceof myPromise) {
//                         res.then(resolve, reject)
//                     } else {
//                         resolve(res)
//                     }
//                 } catch (e) {
//                     reject(e)
//                 }
//             }
//             const rejected = (error) => {
//                 //TODO 这里的逻辑和fulfill的一致
//                 try {
//                     if (!isFunction(onReject)) reject(error);
//                     const res = onReject(error);
//                     if (res instanceof myPromise) {
//                         res.then(resolve, reject)
//                     } else {
//                         resolve(res)
//                     }
//                 } catch (e) {
//                     reject(e)
//                 }
//             }
//             switch (this._status) {
//                 case PENDING:
//                     this._fulfilledQueues.push(fulfilled)
//                     this._rejectedQueues.push(rejected)
//                     break;
//                 case FULFILLED:
//                     fulfilled(this._value)
//                     break;
//                 case REJECTED:
//                     rejected(this._value)
//                     break;
//                 default:
//             }
//         })
//     }
//
//     catch(func) {
//         // TODO 这里忘记返回
//         return this.then(undefined, func)
//     }
//
//     static resolve(result) {
//         if (result instanceof myPromise) return result
//         return new myPromise((resolve) => resolve(result))
//     }
//
//     static reject(error) {
//         return new myPromise((resolve, reject) => reject(error))
//     }
//
//     static all(list) {
//         return new myPromise((resolve, reject) => {
//             const results = [];
//             let count = 0;
//             for (let [i, p] of list.entries()) {
//                 p.then((res) => {
//                     results[i] = res
//                     count++
//                     if (count === list.length) resolve(results)
//                 }, reject)
//             }
//         })
//     }
//
//     static race(list) {
//         return new myPromise((resolve, reject) => {
//             for (let p of list) {
//                 p.then(resolve
//                     , reject)
//             }
//         })
//     }
//
//     static finally(cb) {
//         return this.then((res) => myPromise.resolve(cb()).then(() => res)
//             , (err) => myPromise.resolve(cb()).then(() => {
//                 throw err
//             }))
//     }
// }