const MyPromise = require('./index')

const fakeAjax = (timeout = 0) =>
  new MyPromise((resolve) => {
    setTimeout(() => {
      resolve(`result${Math.random()}`.substring(0, 10))
    }, timeout)
  })

const p1 = new MyPromise((resolve, reject) => {
  setTimeout(() => {
    resolve('myPromise1')
    // reject('error data')
  }, 500)
})

p1.then((data) => {
  console.log('thenData', data)
  return 'new Data'
})
// .then(() => fakeAjax(1000))
// .then((data) => {
//   console.log(data)
//   return data
// })
// .then((data) => {
//   console.log('fahui3', data)
// })

new Promise((resolve, reject) => {
  resolve('res')
})
  .then((data) => {
    console.log('after fin', data)
    return 2
  })
  .finally((data) => {
    console.log('fin', data)
    return 1
  })
  .then((data) => {
    console.log('after fin then', data)
  })

// Promise.resolve(p1)
console.log(process.env.PATH.split(':').join('\n'))

Promise.resolve()