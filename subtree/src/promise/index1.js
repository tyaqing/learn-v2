// const PENDING = 'pending'
// const FULFILLED = 'fulfilled'
// const REJECTED = 'rejected'
// const isFunction = (func) => typeof func === 'function'
//
// class MyPromise {
//   // 当前promise状态
//   _status = PENDING
//   // promise结果值或者错误值
//   _value = undefined
//   // 成功回调队列
//   _fulfilledQueues = []
//   // 失败回调队列
//   _rejectedQueues = []
//
//   constructor(executor) {
//     if (!isFunction(executor))
//       throw new Error('executor should be a function type')
//     try {
//       // 执行传来的函数,并将参数带给外部
//       executor(this._resolve.bind(this), this._reject.bind(this))
//     } catch (e) {
//       this._reject(e)
//     }
//   }
//
//   // 成功处理函数
//   _resolve(result) {
//     const run = () => {
//       // 防止二次执行resolve
//       if (this._status !== PENDING) return
//       // 执行成功队列
//       const runFulfilled = (value) => {
//         while (this._fulfilledQueues.length) {
//           this._fulfilledQueues.shift()(value)
//         }
//       }
//       // 执行失败队列
//       const runRejected = (error) => {
//         while (this._rejectedQueues.length) {
//           this._rejectedQueues.shift()(error)
//         }
//       }
//       // 如果同步函数中返回了promise,则特殊处理
//       if (result instanceof MyPromise) {
//         // 执行返回的promise
//         result.then(
//           (value) => {
//             this._value = value
//             this._status = FULFILLED
//             runFulfilled(value)
//           },
//           (error) => {
//             this._value = error
//             this._status = REJECTED
//             runRejected(error)
//           }
//         )
//       } else {
//         // 执行正常resolve逻辑
//         this._value = result
//         this._status = FULFILLED
//         runFulfilled(result)
//       }
//     }
//     setTimeout(run)
//   }
//   // 失败处理函数
//   _reject(error) {
//     const run = () => {
//       if (this._status !== PENDING) return
//       this._value = error
//       this._status = REJECTED
//       // 执行失败队列
//       while (this._rejectedQueues.length) {
//         this._rejectedQueues.shift()(error)
//       }
//     }
//     setTimeout(run)
//   }
//   // 获取promise resolve结果
//   then(onFulfilled, onRejected) {
//     const { _value, _status } = this
//     return new Promise((resolve, reject) => {
//
//       const fulfilled = (value) => {
//         try {
//           if (!isFunction(onFulfilled)) {
//             // 当参数没有提供函数,直接将值传递到下一个地方 比如.then().then()
//             resolve(value)
//           } else {
//             // ⚠️核心 向then传值 返回的值还需要传给下一个then ps:这里的value是上一次resolve传下来的值
//             const res = onFulfilled(value)
//             // 如果在then函数中返回了promise
//             if (res instanceof MyPromise) {
//               // 执行promise中的then函数
//               res.then(resolve, reject)
//             } else {
//               // 向下传递返回的值
//               resolve(res)
//             }
//           }
//         } catch (e) {
//           reject(e)
//         }
//       }
//       const rejected = (error) => {
//         try {
//           if (!isFunction(onRejected)) {
//             // 当参数没有提供函数,直接将值传递到下一个地方 比如.then().then()
//             reject(error)
//           } else {
//             // ⚠ ️核心 向then传值
//             // ⚠️ 因为reject只是换了个函数执行,如果reject返回值,同样会进入下一个then的resolve
//             const res = onRejected(error)
//             if (res instanceof MyPromise) {
//               res.then(resolve, reject)
//             } else {
//               // 如果onRejected 返回了值,同样需要返回给下一个resolve
//               resolve(res)
//             }
//           }
//         } catch (e) {
//           reject(e)
//         }
//       }
//
//       switch (_status) {
//         // 如果在pending中,则加入到处理队列
//         case PENDING:
//           this._fulfilledQueues.push(fulfilled)
//           this._rejectedQueues.push(rejected)
//           break
//         // 如果已经有结果了,直接返回结果
//         case FULFILLED:
//           fulfilled(_value)
//           break
//         case REJECTED:
//           rejected(_value)
//           break
//         default:
//       }
//     })
//   }
//   // catch也可以在下次进入resolve
//   catch(onRejected) {
//     // 如果任务在进行,catch也会进入待执行队列
//     return this.then(undefined, onRejected)
//   }
//   static resolve(value) {
//     // 如果是promise 直接返回, 否则返回新的promise对象,因为promise自带了then
//     if (value instanceof MyPromise) return value
//     // 否则手动执行Promise
//     return new MyPromise((resolve) => resolve(value))
//   }
//   static reject(value) {
//     return new MyPromise((resolve, reject) => reject(value))
//   }
//   static all(list) {
//     return new MyPromise((resolve, reject) => {
//       // 返回值的集合
//       const values = []
//       let count = 0
//       // eslint-disable-next-line no-restricted-syntax
//       for (const [i, p] of list.entries()) {
//         // 数组参数如果不是MyPromise实例，先调用MyPromise.resolve
//         this.resolve(p).then(
//             // eslint-disable-next-line no-loop-func
//             (res) => {
//               values[i] = res
//               count += 1
//               // 所有状态都变成fulfilled时返回的MyPromise状态就变成fulfilled
//               if (count === list.length) resolve(values)
//             },
//             (err) => {
//               // 有一个被rejected时返回的MyPromise状态就变成rejected
//               reject(err)
//             }
//         )
//       }
//     })
//   }
//   static race(list) {
//     return new MyPromise((resolve, reject) => {
//       // eslint-disable-next-line no-restricted-syntax
//       for (const p of list) {
//         // 只要有一个实例率先改变状态，新的MyPromise的状态就跟着改变
//         this.resolve(p).then(
//             (res) => {
//               resolve(res)
//             },
//             (err) => {
//               reject(err)
//             }
//         )
//       }
//     })
//   }
//   // finally执行完后,值需要继续向下传递
//   finally(cb) {
//     // 需要等上一个任务执行完,所以才then
//     return this.then(
//         // 吧finally的函数执行了,然后继续把值传到下一个then或者catch里面去
//         (value) => MyPromise.resolve(cb()).then(() => value),
//         (reason) =>
//             MyPromise.resolve(cb()).then(() => {
//               throw reason
//             })
//     )
//   }
// }
//
// module.exports = MyPromise
