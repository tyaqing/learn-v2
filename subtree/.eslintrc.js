module.exports = {
  env: {
    es6: true,
    browser: true
  },
  extends: ['airbnb-base', 'prettier', 'plugin:react/recommended'],
  parser: 'babel-eslint',

  parserOptions: {
    ecmaVersion: 10,
    sourceType: 'module'
  },
  rules: {
    'no-console': 0,
    'no-useless-escape': 0,
    'no-multiple-empty-lines': [0],
    'no-extend-native': 0,
    'func-names': 0,
    'no-proto': 0,
    'no-param-reassign': 0,
    'lines-between-class-members': 0,
    'no-underscore-dangle': 0,
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        semi: false,
        trailingComma: 'none',
        bracketSpacing: true,
        jsxBracketSameLine: true,
        endOfLine: 'auto'
      }
    ]
  },
  plugins: ['prettier']
}
