// eslint-disable-next-line import/no-extraneous-dependencies
const babel = require('@babel/core')

module.exports = function BabelLoader(source) {
  const res = babel.transform(source, {
    sourceType: 'module' // 允许使用ES6 import和export语法
  })
  // console.log(source)
  return res.code
}
