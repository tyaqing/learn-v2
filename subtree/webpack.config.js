const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
// const BabelLoader = require('./BabelLoader')
const TestPlugin = require('./TestPlugin')

const resolve = (dir) => path.join(__dirname, dir)
module.exports = {
  entry: {
    app: './src/index.js'
    // underscore: './src/underscore/index.js'
  },
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    path: resolve('dist')
  },
  devServer: {
    useLocalIp: true
  },
  // devtool: 'inline-source-map',
  plugins: [
    new TestPlugin(), // 一个测试plugin
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Development',
      template: 'src/index.html'
    })
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        // include: [resolve('src')],
        use: {
          loader: path.resolve('./BabelLoader')
        }
      }
      // {
      //   test: /\.worker\.js$/,
      //   use: { loader: 'worker-loader' }
      // }
      // {
      //   test: /\.m?js$/,
      //   exclude: /(node_modules|bower_components)/,
      //   use: {
      //     loader: 'babel-loader',
      //     options: {
      //       presets: ['@babel/preset-env'],
      //       plugins: [
      //         // '@babel/plugin-transform-arrow-functions',
      //         ['@babel/plugin-proposal-decorators', { legacy: true }] // 这里是关键
      //         // ['@babel/plugin-proposal-class-properties', { loose: false }] // 这个是编译类的方法
      //       ]
      //     }
      //   }
      // }
    ]
  }
}
